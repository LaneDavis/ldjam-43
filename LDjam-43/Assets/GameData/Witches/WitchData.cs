﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Witch", menuName = "GameData/Witch")]
public class WitchData : ScriptableObject {

    public Sprite Sprite;
    public string DisplayName = "Glenda";
    public Color PortraitColor = Color.yellow;
    public Color FrameColor = Color.yellow;
    public Color BackgroundColor = Color.yellow;
    public Color TextColor = Color.yellow;
    

}
