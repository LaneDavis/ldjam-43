﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Dialog Data", menuName = "GameData/Dialog Data")]
public class DialogData : ScriptableObject {

	public List<DialogLine> LevelLines;
	
}
