﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[CreateAssetMenu(fileName = "DialogLine", menuName = "GameData/DialogLine")]
public class DialogLine : ScriptableObject {

	public WitchData Witch;
	public SoundCall SoundCall;
	
	[TextArea]
	public string TextString;

	public int LevelNum;

	public enum DialogTrigger {
		Death,
		LevelStart,
		Restart,
		Idle
	}

	public DialogTrigger Trigger = DialogTrigger.Death;
	
	public void SetupLine() {
		if (SoundCall.soundUsed == null) return;
		SoundCall.priority = 1;
		SoundCall.volume = 1f;
		SoundCall.pitch = 1f;
	}

	public float textSpeedMultiplier = 1f;
	
	[HideInInspector] public bool PlayerHasHeardLine = false;

}