﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelList", menuName = "GameData/LevelList")]
public class LevelList : ScriptableObject {

	public List<string> Levels;

}
