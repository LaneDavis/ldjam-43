﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class EndingController : MonoBehaviour {

	public Animator Animator;
	public TextMeshProUGUI TimeText;
	public TextMeshProUGUI DeathsText;

	public void StartEnding() {
		Animator.Play("Fade In");
		TimeText.text = "<color=yellow>Time: </color>" + TimeCounter.Instance.GetComponent<TextMeshProUGUI>().text;
		DeathsText.text = "<color=yellow>Deaths: </color>" + DeathCounter.Instance.DeathCount;
	}
	
	public string TimeString() {

		float totalSeconds = Time.time;
		
		int hours = (int) totalSeconds / 3600;
		if (hours > 0) totalSeconds -= hours * 3600;
		int minutes = (int) totalSeconds / 60;
		totalSeconds -= minutes * 60;
		float overflow = totalSeconds % 1f;
		int intSeconds = (int) totalSeconds;

		if (hours > 0) {
			return hours.ToString("D2") + ":" + minutes.ToString("D2") + ":" + intSeconds.ToString("D2") + overflow.ToString("F2").Substring(1, 3);
		} else {
			return minutes.ToString("D2") + ":" + intSeconds.ToString("D2") + overflow.ToString("F2").Substring(1, 3);
		}
		
	}

}
