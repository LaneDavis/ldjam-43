using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

public abstract class GOCustomizableTile<T> : TileBase
{
    public Sprite Sprite;
    public override void RefreshTile(Vector3Int position, ITilemap tilemap)
    {
        base.RefreshTile(position, tilemap);
        var scene = tilemap.GetComponent<Transform>().gameObject.scene;
        if (scene.name == "Preview Scene")
            return;
        if (tilemap.GetTile(position) != this)
            DestroyAt(position);
        else
            EnsureExistsAt(position);
    }

    protected abstract void DestroyAt(Vector3 position);
    protected abstract void EnsureExistsAt(Vector3 position);
    protected abstract T GetAt(Vector3 position);
    protected abstract void ApplyCustomization(T info, ref TileData tileData);
    protected abstract void ClearCustomization(ref TileData tileData);

    public override void GetTileData(Vector3Int position, ITilemap tilemap, ref TileData tileData)
    {
        base.GetTileData(position, tilemap, ref tileData);
        var info = GetAt(position);
        tileData.sprite = Sprite;
        if (info != null)
        {
            ApplyCustomization(info, ref tileData);
        }
        else
        {
            ClearCustomization(ref tileData);
        }
    }

}