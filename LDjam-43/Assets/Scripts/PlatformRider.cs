﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets._2D;

public class PlatformRider : MonoBehaviour {
	private Rigidbody2D _body;
	private PlatformerCharacter2D _character;

	private GameObject _ridingPlatform;
	private Vector3 _ridingPlatformPrevPos;

	private Vector2 _prevInstantaneousVelocity;
	
	private void Start()
	{
		_body = GetComponent<Rigidbody2D>();
		_character = GetComponent<PlatformerCharacter2D>();
	}

	private void FixedUpdate()
	{
		RideOnPlatform();
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Platform"))
		{
			_ridingPlatform = other.gameObject;
			LandOnPlatform();
		}
	}

	private void OnTriggerExit2D(Collider2D other)
	{
		if (other.gameObject == _ridingPlatform)
		{
			_ridingPlatform = null;
			ExitPlatform();
		}
	}

	private void LandOnPlatform()
	{
		_ridingPlatformPrevPos = _ridingPlatform.transform.position;
		_prevInstantaneousVelocity = Vector2.zero;
		if (_character == null)
		{
			_body.constraints &= ~RigidbodyConstraints2D.FreezePositionX;
		}
	}

	private void ExitPlatform()
	{
		if (_character == null)
		{
			_body.constraints |= RigidbodyConstraints2D.FreezePositionX;
		}
		
	}

	private void RideOnPlatform()
	{
		if (_ridingPlatform == null)
			return;
		Vector3 delta = _ridingPlatform.transform.position - _ridingPlatformPrevPos;
		_ridingPlatformPrevPos = _ridingPlatform.transform.position;
		delta.y = -Mathf.Abs(delta.y);

		if (_character)
		{
			_character.ExtraInstantaneousVelocity = delta;
		}
		else
		{
			Vector2 scaledDelta = (Vector2)delta / Time.deltaTime;
			_body.velocity -= _prevInstantaneousVelocity;
			_body.velocity += scaledDelta;
			_prevInstantaneousVelocity = scaledDelta;
		}
	}
}
