﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Rewired;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TransitionController : MonoBehaviour {

	public static TransitionController Instance;
	
	public Camera RealCamera;
	public LevelList LevelList;
	private List<string> Levels { get { return LevelList.Levels; } }

	public RectTransform SpotlightRT;
	public Animator SpotlightAnimator;

	[HideInInspector] public bool IsDoingTransition = false;

	public static int lastLevel = -1;

	public EndingController EndingController;

	[HideInInspector] public float TimeOfLastRestart;

	private void Awake() { Instance = this;}
	
	private void Start() {

		SceneManager.sceneLoaded += StartLevelIntro;
		
		// If no levels are loaded, load the first one
		if (SceneManager.sceneCount <= 1) {
			LoadLevel(0);
		}
//		if (string.IsNullOrEmpty(CurrentLevelScene())) {
//			LoadLevel(0);
//		}
		
	}

	private void LoadLevel(int levelNum) {

		if (!string.IsNullOrEmpty(CurrentLevelScene())) {
			SceneManager.UnloadSceneAsync(CurrentLevelScene());
		}
		
		IsDoingTransition = true;
		
		if (levelNum >= Levels.Count || levelNum < 0) {
			Debug.LogError("Level does not exist!");
			
			// But oops! That's totally just the end of the game!   
			EndingController.StartEnding();
		}

		lastLevel = levelNum;
		DialogManager.Instance.QueueNextLevelStartDialog();
		SceneManager.LoadScene(Levels[levelNum], LoadSceneMode.Additive);
	}

	private string CurrentLevelScene() {
		for (int i = 0; i < SceneManager.sceneCount; i++) {
			for (int j = 0; j < Levels.Count; j++) {
				if (SceneManager.GetSceneAt(i).name == Levels[j]) {
					return Levels[j];
				}
			}
		}

		return "";
	}

	private void Update() {

		if (Initialize.Instance.HasVisualIntroToConsume) {
			VisualIntro();
			int levelNum = NumberOfLevel(Initialize.Instance.InitializedOnLevel);
			if (levelNum >= 0 && levelNum < Levels.Count) lastLevel = levelNum;
			Initialize.Instance.HasVisualIntroToConsume = false;
		}
		
		// Position the spotlight 
		if (PlayerManager.Instance.PlayerObject != null) {
			SpotlightRT.transform.position =
				Camera.main.WorldToScreenPoint(PlayerManager.Instance.PlayerObject.transform.position);
		}

	}
	
	public void StartLevelOutro(Vector3 doorPosition) {

		IsDoingTransition = true;
		
		// Animate the spotlight in
		SpotlightAnimator.Play("Level Outro");
		
	}

	private void StartLevelIntro(Scene scene, LoadSceneMode mode) {
		
		// If the level list doesn't contain this scene, we won't do an intro, since this isn't a level.
		if (!Levels.Contains(scene.name)) return;

		Invoke("VisualIntro", 0.5f);

	}

	public void VisualIntro() {
		
		// Snap the camera to the character
		ChasePlayer.Instance.SnapToPlayer();
		
		// Position the spotlight on the character
		if (PlayerManager.Instance.PlayerObject != null) {
			SpotlightRT.transform.position = Camera.main.WorldToScreenPoint(PlayerManager.Instance.PlayerObject.transform.position);
		}
		
		// Play the Intro Animation
		SpotlightAnimator.Play("Level Intro");
		
	}
	
	// From Spotlight animation
	public void FinishIntro() {
		IsDoingTransition = false;
	}
	
	// From Spotlight animation
	public void FinishOutro() {

		// Load next level if possible
		if (Levels.Count >= lastLevel) {
			LoadLevel(lastLevel + 1);
		} 
		
		// Otherwise, do some kind of victory celebration
		else {
			EndingController.StartEnding();
		}
		
	}

	public string NameOfLevelNumber(int number) {
		if (number < 0 || number >= Levels.Count) return "";
		return Levels[number];
	}

	public int NumberOfLevel(string levelName) {
		for (int i = 0; i < Levels.Count; i++) {
			if (Levels[i] == levelName) return i;
		}

		return -1;
	}

	public void ResetLevel() {
		SpotlightAnimator.Play("Dark");
		DialogManager.Instance.QueueNextRestartDialog();
		LoadLevel(NumberOfLevel(CurrentLevelScene()));
		TimeOfLastRestart = Time.time;
	}

}
