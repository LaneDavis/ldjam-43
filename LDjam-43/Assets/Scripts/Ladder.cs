﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class Ladder : MonoBehaviour
{
	private AreaEffector2D _effector;
	private BoxCollider2D _trigger;
	public float UpForce = 50;
	public float StaticForce = 29.5f;
	public float DownForce = 20f;

	private void Awake()
	{
		_effector = GetComponent<AreaEffector2D>();
		_trigger = GetComponent<BoxCollider2D>();
	}

	private void Start()
	{
		var allLadders = FindObjectsOfType<Ladder>().Where(e=>e.transform.position.x == transform.position.x);
		Bounds bounds = _trigger.bounds;
		foreach (var ladder in allLadders)
		{
			if (ladder == this)
				continue;
			bounds.Encapsulate(ladder._trigger.bounds);
			Destroy(ladder._effector);
			Destroy(ladder._trigger);
			Destroy(ladder);
		}
		_trigger.offset = bounds.center - _trigger.transform.position;
		_trigger.size = bounds.size;
	}

	void Update ()
	{
		float v = Input.GetAxis("Vertical");
		_effector.forceMagnitude = v > 0 ? UpForce : v < 0 ? DownForce : StaticForce;
	}
}
