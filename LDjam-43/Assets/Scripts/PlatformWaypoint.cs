﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

[ExecuteInEditMode]
public class PlatformWaypoint : MonoBehaviour
{
    private static HashSet<PlatformWaypoint> _waypoints = new HashSet<PlatformWaypoint>();
    
    public static PlatformWaypoint GetPathAt(Vector2 pos)
    {
        Vector2Int intPos = new Vector2Int(Mathf.RoundToInt(pos.x), Mathf.RoundToInt(pos.y));
        return _waypoints.FirstOrDefault(e => e.IntPos == intPos);
    }
    
    public PlatformWaypoint Next;

    private PlatformWaypoint _prev;
    private void OnDrawGizmos()
    {
        if (Next != null)
            Gizmos.DrawLine(transform.position, Next.transform.position);
    }

    private void OnValidate()
    {
        if (!Next)
            Next = this;
        Next._prev = this;
    }

    private Vector2Int IntPos
    {
        get { return new Vector2Int(Mathf.RoundToInt(transform.position.x), Mathf.RoundToInt(transform.position.y)); }
    }

    private void OnDestroy()
    {
        _waypoints.Remove(this);
        
        if (!Application.isPlaying)
        {
            if (_prev != null)
                _prev.Next = Next;
            if (Next != null)
                Next._prev = _prev;
        }
    }

    private void Awake()
    {
        if (!Next)
            Next = this;
        Next._prev = this;
        _waypoints.Add(this);
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(PlatformWaypoint))]
public class PlatformWaypointEditor : Editor
{
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Add Point"))
        {
            var curWaypoint = (PlatformWaypoint)target;
            var obj = Instantiate(curWaypoint);
            obj.Next = curWaypoint.Next;
            curWaypoint.Next = obj;
            obj.transform.position += Vector3.up;
            obj.name = "Waypoint";
            Selection.activeObject = obj;
        }
    }
}


#endif