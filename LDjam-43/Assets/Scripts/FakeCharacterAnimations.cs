﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FakeCharacterAnimations : MonoBehaviour {

	[Header("Walking")]
	public FxPackageController FxLeftFast;
	public FxPackageController FxLeftSlow;
	public FxPackageController FxRightFast;
	public FxPackageController FxRightSlow;

	public float fastThreshold;
	public float movePerPulse;
	private float unspentMove;
	private bool nextIsRight = true;

	[Header("Jumping")] 
	public FxPackageController FxJump;
	public SoundCall JumpSound;
	private const float JumpSoundCooldown = 0.25f;
	private float timeOfNextJumpSound = 0f;

	public void WalkAnimation(float horizontalMove) {
		unspentMove += Mathf.Abs(horizontalMove) * Time.deltaTime;
		if (unspentMove > movePerPulse) {
			unspentMove = 0f;
			if (nextIsRight) {
				if (Mathf.Abs(horizontalMove * Time.deltaTime) > fastThreshold) {
					FxRightFast.TriggerAll();
				} else {
					FxRightSlow.TriggerAll();
				}
			} else {
				if (Mathf.Abs(horizontalMove * Time.deltaTime) > fastThreshold) {
					FxLeftFast.TriggerAll();
				} else {
					FxLeftSlow.TriggerAll();
				}
			}

			nextIsRight = !nextIsRight;
		}
 	}

	public void JumpAnimation() {
		FxJump.TriggerAll();
		if (Time.time > timeOfNextJumpSound) {
			timeOfNextJumpSound = Time.time + JumpSoundCooldown;
			SoundManager.instance.PlaySound(JumpSound, gameObject);
		}
	}

}
