﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

public class CatDoorInteraction : MonoBehaviour {
	public void AnimateThroughDoor(Transform position, bool enter)
	{
		if (enter)
		{
			ComeIn(position);
		}
		else
		{
			Leave(position);
		}
	}

	private void Leave(Transform door)
	{
		GetComponent<Rigidbody2D>().simulated = false;
		GetComponent<CatRecorder>().enabled = false;
		StartCoroutine(AnimateTowardExit(door));
	}

	private IEnumerator AnimateTowardExit(Transform door) {
		Vector3 myPosition = transform.position;
		transform.parent.position = myPosition;
		transform.position = myPosition;
		transform.localPosition = new Vector3(0f, 0.5f, 0f);
		Vector3 start = transform.parent.position + Vector3.up * 0.0f;
		Vector3 end = door.transform.position;
		for (float t = 0; t < .25f; t += Time.deltaTime)
		{
			transform.parent.position = Vector3.Lerp(start, end, t / .25f);
			yield return null;
		}

		GetComponent<Animator>().enabled = true;
		GetComponent<Animator>().Play("Exit");
	}

	private void ComeIn(Transform door)
	{
		transform.parent.position = door.transform.position + Vector3.right;
		GetComponent<Rigidbody2D>().simulated = false;
		GetComponent<Animator>().Play("Entry");
		GetComponent<CatRecorder>().enabled = false;
	}

	public void DoneComingIn()
	{
		GetComponent<Rigidbody2D>().simulated = true;
		GetComponent<Animator>().enabled = false;
		GetComponent<CatRecorder>().enabled = true;
	}
}
