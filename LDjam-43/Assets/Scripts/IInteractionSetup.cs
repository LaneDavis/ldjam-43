﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractionSetup
{
    void SetupInteraction(List<GameObject> targets);
}
