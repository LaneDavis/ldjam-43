﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[SelectionBase]
public class GhostCat : MonoBehaviour
{
	public GameObject DeadBody;
	public GameObject GhostBody;
	
	private Vector2[] _recordedPath = new Vector2[0];
	private bool[] _recordedDirections = new bool[0];
	private int _index;

	private void FixedUpdate()
	{
		if (_index < _recordedPath.Length)
		{
			transform.position = _recordedPath[_index];
			Vector3 scale = Vector3.one;
			if (!_recordedDirections[_index])
				scale.x = -1;
			GhostBody.transform.localScale = scale;
		}
		else
		{
			GhostBody.transform.localPosition = Vector3.zero;
			DeadBody.transform.localPosition = Vector3.zero;
			GhostBody.SetActive(false);
			DeadBody.SetActive(true);
			enabled = false;
			Vector3 pos = transform.position;
			pos.x -= .5f;
			pos.y -= .5f;
			pos.x = Mathf.Round(pos.x);
			pos.y = Mathf.Round(pos.y);
			pos.x += .5f;
			pos.y += .5f;
			transform.position = pos;
		}
		++_index;
	}

	public void ResetLevel()
	{
		DeadBody.SetActive(false);
		GhostBody.SetActive(true);
		enabled = true;
		_index = 0;
	}

	public void Playback(CatRecorder recorder)
	{
		recorder.CopyAndResetPath(out _recordedPath, out _recordedDirections);
	}
}
