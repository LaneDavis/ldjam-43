﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
	public float Speed = 1;
	
	private void Start()
	{
		GetComponent<Rigidbody2D>().velocity = transform.forward * Speed;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.CompareTag("Ghost"))
			return;
		if (SFXObjectManager.Instance != null) {
			SFXObjectManager.Instance.MakeSFXAtPoint("Cannonball Collision SFX", transform.position);
		}
		Destroy(gameObject);
	}

	private void ResetLevel()
	{
		Destroy(gameObject);
	}
}
