﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChasePlayer : MonoBehaviour {

	public static ChasePlayer Instance;
	
	public AnimationCurve ChaseCurve;

	private GameObject curCat;
	public Vector3 Offset = new Vector3(0f, 2.2f, -6.35f);

	private void Awake() { Instance = this;}
	
	private void Update() {
		Chase();
	}
	
	public void SnapToPlayer() {
		Vector3 targetPosition = PlayerManager.Instance.PlayerObject.transform.position + Offset;
		transform.position = targetPosition;
	}

	private void Chase() {
		if (PlayerManager.Instance.PlayerObject != null && PlayerManager.Instance.IsAlive) {
			Vector3 targetPosition = PlayerManager.Instance.PlayerObject.transform.position + Offset;
			float dist = Vector3.Distance(targetPosition, transform.position);
			Vector3 dir = (targetPosition - transform.position).normalized;
			transform.Translate(dir * ChaseCurve.Evaluate(dist) * Time.deltaTime);
		}
	}
}
