﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Forcefield : MonoBehaviour
{
    private Collider2D[] _collider2Ds;
    private Renderer _renderer;
    private Color _fullColor;
    private Color _hollowColor;

    public bool IsOn = true;
    
    public ParticleSystem Particles;
    
    private void Start()
    {
        _collider2Ds = GetComponentsInChildren<Collider2D>();
        _renderer = GetComponentInChildren<Renderer>();
        _fullColor = _renderer.material.color;
        _hollowColor = _fullColor;
        _hollowColor.a /= 2;
    }

    public void TurnOn() {
        IsOn = true;
        Particles.Stop();
        _renderer.material.color = _hollowColor;
        foreach (var collider in _collider2Ds)
        {
            collider.enabled = false;
        }
    }

    public void TurnOff()
    {
        IsOn = false;
        Particles.Play();
        _renderer.material.color = _fullColor;
        foreach (var collider in _collider2Ds)
        {
            collider.enabled = true;
        }
    }
}
