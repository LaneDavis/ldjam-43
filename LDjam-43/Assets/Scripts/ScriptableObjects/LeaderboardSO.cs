﻿using System;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "LeaderboardData", order = 1)]
[Serializable]
public class LeaderboardSO : ScriptableObject
{
	public int Level;
	public List<LeaderboardSubmission> submissions;
}

[Serializable]
public class LeaderboardSubmission
{
	public string PlayerName;
	public int Level;
	public float LevelTime;
	public int NumDeaths;
	public long Score; // Calculated when submitted

	public LeaderboardSubmission(string playerName, int level, float levelTime, int numDeaths)
	{
		PlayerName = playerName ?? "anonymous";
		Level = level;
		LevelTime = levelTime;
		NumDeaths = numDeaths;
	}
}