﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.Tilemaps;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class WorldGenerator : MonoBehaviour {

	public List<BlockType> BlockTypes = new List<BlockType>();
	public Tilemap Tilemap;
	public Tilemap Interactions;
	public List<GameObject> DuplicatedEnvironmentBlocks;

	[FormerlySerializedAs("TileObjectTransform")] public Transform TileObjectHolder;
	
	private const int maxWorldX = 200;
	private const int maxWorldY = 100;

	private bool hasTiles = false;
	
	// Some blocks are duplicated, so third dimension
	public GameObject[,,] TileObjects = new GameObject [maxWorldX,maxWorldY,2];
		
	// See this: https://forum.unity.com/threads/tilemap-tile-positions-assistance.485867/
	public void GenerateWorld() {
		
		if (!hasTiles) GrabTiles();

		ClearWorld();

		Dictionary<TileBase, List<GameObject>> interactions = new Dictionary<TileBase, List<GameObject>>();
		
		for (int x = -maxWorldX / 2; x < maxWorldX / 2; x++)
		{
			for (int y = -maxWorldY / 2; y < maxWorldY; y++)
			{
				var pos = new Vector3Int(x, y, 0);
				TileBase tb = Tilemap.GetTile(pos);
				if (tb != null)
				{
					var interaction = Interactions.GetTile(pos);
					var block = BlockTypes.FirstOrDefault(e=>e.tile == tb);
					if (block != null)
					{
						#if UNITY_EDITOR
						
						var obj = (GameObject)PrefabUtility.InstantiatePrefab(block.obj);
						obj.transform.position = new Vector3(x, y, 0);
						obj.transform.parent = TileObjectHolder;
						Undo.RegisterCreatedObjectUndo(obj, "Regenerate World");

						if (DuplicatedEnvironmentBlocks.FirstOrDefault(e=>e.name == obj.name) != null)
						{
							// If this is a wall or floor, duplicate to add depth
							var obj2 = (GameObject) PrefabUtility.InstantiatePrefab(block.obj);
							obj2.transform.position = new Vector3(x, y, 0.3f);
							obj2.transform.parent = TileObjectHolder;
							Undo.RegisterCreatedObjectUndo(obj2, "Regenerate World");
						
							// Also shift the other object back a bit
							obj.transform.position += new Vector3(0.0f, 0.0f, -0.3f);
							
							TileObjects[x + maxWorldX / 2, y + maxWorldY / 2, 1] = obj2;
						}
						
						#else
						var obj = Instantiate(block.obj, new Vector3(x, y, 0), Quaternion.identity, TileObjectHolder);
						#endif
						TileObjects[x + maxWorldX / 2, y + maxWorldY / 2, 0] = obj;
						if (interaction != null)
						{
							List<GameObject> targets;
							if (!interactions.TryGetValue(interaction, out targets))
								interactions[interaction] = targets = new List<GameObject>();
							targets.Add(obj);
						}
					}
				}
			}
		}

		foreach (var interaction in interactions.Values)
		{
			foreach (var obj in interaction)
			{
				var triggerButton = obj.GetComponentInChildren<IInteractionSetup>();
				if (triggerButton != null)
					triggerButton.SetupInteraction(interaction);
			}
		}

		hasTiles = true;

	}

	private void Start()
	{
		Tilemap.gameObject.SetActive(false);
		Interactions.gameObject.SetActive(false);
	}

/*
	private void OnValidate()
	{
		foreach (var blockType in BlockTypes)
		{
			blockType.tile.gameObject = blockType.obj;
		}
	}
*/
	public void ClearWorld() {

		if (!hasTiles) GrabTiles();
				
		for (int x = 0; x < maxWorldX; x++) {
			for (int y = 0; y < maxWorldY; y++) {
				for (int layer = 0; layer < 2; layer++) { // Some blocks created twice
					if (TileObjects[x, y, layer] != null) {
						#if UNITY_EDITOR
						Undo.DestroyObjectImmediate(TileObjects[x, y, layer]);
						#else
						DestroyImmediate(TileObjects[x, y, layer]);
						#endif
					}	
				}
			}
		}
		
		hasTiles = true;
	}

	private void GrabTiles() {
		foreach (Transform t in TileObjectHolder) {
			if (Mathf.Approximately(t.transform.position.z, -0.3f))
			{
				// There were two blocks, and this one was in the first slot
				TileObjects[Mathf.RoundToInt(t.position.x + maxWorldX / 2),
					Mathf.RoundToInt(t.position.y + maxWorldY / 2), 0] = t.gameObject;
			}
			else if (Mathf.Approximately(t.transform.position.z, 0.3f))
			{
				// There were two blocks, and this one was in the second slot
				TileObjects[Mathf.RoundToInt(t.position.x + maxWorldX / 2),
					Mathf.RoundToInt(t.position.y + maxWorldY / 2), 1] = t.gameObject;
			}
			else
			{
				// Normal (one block)
				TileObjects[Mathf.RoundToInt(t.position.x + maxWorldX / 2),
					Mathf.RoundToInt(t.position.y + maxWorldY / 2), 0] = t.gameObject;				
			}
		}

		hasTiles = true;
	}
	
}

[System.Serializable]
public class BlockType {
	public TileBase tile;
	public GameObject obj;
}

#if UNITY_EDITOR
[CustomEditor(typeof(WorldGenerator))]
public class WorldGeneratorEditor : UnityEditor.Editor
{		
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
        
		var worldGenerator = (WorldGenerator)target;
		if(GUILayout.Button("Regenerate World")) {
			worldGenerator.GenerateWorld();
		}

		if (GUILayout.Button("Clear World")) {
			worldGenerator.ClearWorld();
		}
	}
}
#endif