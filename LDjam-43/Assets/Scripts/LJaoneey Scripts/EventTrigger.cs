﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventTrigger : MonoBehaviour {

	public UnityEvent Event;

	public void TriggerEvents() {
		Event.Invoke();
	}

	public List<UnityEvent> EventList = new List<UnityEvent>();

	public void TriggerEventsByNumber(int num) {
		EventList[num].Invoke();
	}
	
}
