﻿using UnityEngine;

public class FxPackage : MonoBehaviour {

	[HideInInspector] public FxPackageController Controller;
	[HideInInspector] public bool ToggleState;

	protected GameObject Anchor;

	public static int NextFxId;
	protected int SfxId = -1;

	public virtual void Trigger (GameObject newAnchor = null) {

		IncrementSfxId();

		if (newAnchor != null)
			Anchor = newAnchor;
		
	}

	public virtual void Toggle (bool toggleState, GameObject newAnchor = null) {

		if (!ToggleState && toggleState) {
			IncrementSfxId();
		}

		ToggleState = toggleState;
		if (newAnchor != null)
			Anchor = newAnchor;
		
	}

	public virtual void SetUp (GameObject newAnchor = null) {

		//Called by the SFXPackageController's Start() function.

	}

	private void IncrementSfxId () {

		SfxId = NextFxId;
		NextFxId++;

	}

}
