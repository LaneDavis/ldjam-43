﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class FxApplierColor : FxApplier {
	private Color baseColor;
	
	private RawImage rawImageTarget;
	private Image imageTarget;
	private SpriteRenderer spriteTarget;
	private Text textTarget;
	private TextMeshProUGUI textMeshTarget;

	private readonly List<ColorWeight> factorWeights = new List<ColorWeight>();
	private Color finalColor;
	
	public void SetUp (GameObject newAnchor = null) {
		if (newAnchor == null) return;

		rawImageTarget = GetComponent<RawImage>();
		imageTarget = GetComponent<Image>();
		spriteTarget = GetComponent<SpriteRenderer>();
		textTarget = GetComponent<Text>();
		textMeshTarget = GetComponent<TextMeshProUGUI>();

		if (rawImageTarget != null) baseColor = rawImageTarget.color;
		else if (imageTarget != null) baseColor = imageTarget.color;
		else if (spriteTarget != null) baseColor = spriteTarget.color;
		else if (textTarget != null) baseColor = textTarget.color;
		else if (textMeshTarget != null) baseColor = textMeshTarget.color;
		
	}

	public void AddColorFactor (int sfxId, float newValue, float newDecayRate, Color newColor) {

		FxFactorColor factor = (FxFactorColor)FactorWithId(sfxId);
		if(factor != null) {
			factor.Value = newValue;
			factor.Color = newColor;
		} else {
			factor = new FxFactorColor {
				SfxId = sfxId,
				Value = newValue,
				DecayRate = newDecayRate
			};
			factor.Color = newColor;
			ActiveFactors.Add(factor);
		}

	}

	private void Update () {

		ApplyColor();
		AgeFactors();

	}

	private void ApplyColor () {

		factorWeights.Clear();
		factorWeights.Add(new ColorWeight(baseColor, 1f));
		foreach (FxFactor factor in ActiveFactors) {
			factorWeights.Add(new ColorWeight(((FxFactorColor) factor).Color, factor.Value));
		}

		finalColor = ColorUtilities.BlendColors(factorWeights);

		if (rawImageTarget != null) rawImageTarget.color = finalColor;
		else if (imageTarget != null) imageTarget.color = finalColor;
		else if (spriteTarget != null) spriteTarget.color = finalColor;
		else if (textTarget != null) textTarget.color = finalColor;
		else if (textMeshTarget != null) textMeshTarget.color = finalColor;
		
	}

	public void SetBaseColor(Color color) {
		baseColor = color;
	}

}

public class FxFactorColor : FxFactor {

	public Color Color;
	
}

public static class ColorUtilities {

	/// <summary>
	/// Blends the colors.
	/// </summary>
	/// <returns>Black for an empty list, otherwise a blended color.</returns>
	/// <param name="colors">Colors.</param>
	public static Color BlendColors(List<ColorWeight> colors, float forceAlpha = -1f) {
		if (colors.Count == 0)
			return Color.black;
		float totalWeight, r, g, b, a;
		totalWeight = r = g = b = a = 0f;
		for (int i = 0; i < colors.Count; i++) {
			totalWeight += colors[i].weight;
			r += colors[i].color.r * colors[i].weight;
			g += colors[i].color.g * colors[i].weight;
			b += colors[i].color.b * colors[i].weight;
			a += colors[i].color.a * colors[i].weight;
		}
		return new Color(r / totalWeight, g / totalWeight, b / totalWeight, Mathf.Abs(forceAlpha - (-1f)) > 0.01f ? forceAlpha : a / totalWeight);
	}

}

public class ColorWeight {
	public Color color;
	public float weight;
	public ColorWeight(Color color, float weight) {
		this.color = color; this.weight = weight;
	}
}
