﻿

using UnityEngine;

public class FxScreenshake : FxPackage {

	//FACTOR ATTRIBUTES
	[Range(0F, 25f)] public float DecayRate = 12f;
	[Range(0F, 1f)] public float ValueMultiplier = .3f;
	public AnimationCurve ValueCurve = new AnimationCurve (new Keyframe (0f, 1f), new Keyframe (1f, 1f)); 

	private void Update () {

		//If toggled on, set the shake value based on the curve.
		if (ToggleState) {
//			MenuScreenshakeManager.Instance.SetScreenshake(SfxId, ValueCurve.Evaluate(Controller.Timer) * ValueMultiplier, DecayRate);
		}

	}

	public override void Trigger (GameObject newAnchor = null) {
		base.Trigger(newAnchor);
//		MenuScreenshakeManager.Instance.SetScreenshake(SfxId, ValueCurve.Evaluate(0F) * ValueMultiplier, DecayRate);
	}

}
