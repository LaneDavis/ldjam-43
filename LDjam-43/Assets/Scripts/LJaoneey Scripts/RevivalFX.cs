﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class RevivalFX : SFXObject {

	public AnimationCurve BloomCurve;

	public override void StartFX() {
		base.StartFX();
		
		RumbleManager.Instance.AddTrauma(2f, 0f);
		Camshake.Instance.AddTrauma(1.5f);
	}

	public override void Update() {
		base.Update();
		if (isOn) {
			PPAtlas.Ins.Revival.weight = BloomCurve.Evaluate(Progress);
		}
	}

	

}
