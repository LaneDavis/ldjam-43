﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Mime;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class DialogBoxController : MonoBehaviour {

	public static DialogBoxController Instance;
	
	public enum State {
		Hidden,
		FadingIn,
		Idle,
		FadingOut
	}

	private State curState = State.Hidden;
	
	public Animator Animator;
	public TextMeshProUGUI Text;
	private List<DialogStep> queue = new List<DialogStep>();
	private DialogStep curStep;
	private float timer;
	[HideInInspector] public bool IsDoingSequence;

	private float clipEndTime = 0.4f;
	private float textSpeedMultiplier = 2f;

	[Header("Configurable Elements")]
	public Image WitchPortrait;
	public Image TextBackground;
	public List<Image> Borders;
	public Image PortraitBackground;
	public TextMeshProUGUI WitchNameText;
	public RectTransform DialogStretcher;

	private float StretcherBaseWidth = 228f;
	private float StretcherWidthPerCharacter = 16f;

	[Header("Music Duck")]
	public MusicFactor MusicDuck;
	private bool ducked;

	private float CurStepProgress {
		get {
			if (curStep == null) return 0f;
			return Mathf.Clamp01(timer / curStep.Duration);
		}
	}

	private void Awake() { Instance = this; }

	public void QueueDialog(DialogLine line) {
		
		queue.Add(new DialogStep(line, line.SoundCall.soundUsed.GetComponent<AudioSource>().clip.length + clipEndTime));
		if (curStep == null) {
			StepForward();
		}
		
	}

	private void StepForward() {

		timer = 0f;
		
		if (queue.Count > 0) {
			curStep = queue[0];
			queue.RemoveAt(0);
			
			// Play Sound
			SoundManager.instance.PlaySound(curStep.Line.SoundCall, SoundManager.instance.gameObject);
			if (!ducked) MusicManager.Instance.AddFactor(MusicDuck);
			ducked = true;

			// Theme the dialog for the witch
			WitchPortrait.sprite = curStep.Line.Witch.Sprite;
			TextBackground.color = curStep.Line.Witch.BackgroundColor;
			foreach (Image border in Borders) border.color = curStep.Line.Witch.FrameColor;
			PortraitBackground.color = curStep.Line.Witch.PortraitColor;
			WitchNameText.text = curStep.Line.Witch.DisplayName;
			WitchNameText.color = curStep.Line.Witch.TextColor;
			
			if (curState == State.Hidden) {
				IsDoingSequence = true;
				AnimateIn();
			}
			
		} else {		// No more messages to display.
			if (curState == State.Idle || curState == State.FadingIn) {
				AnimateOut();
			}
		}
		
	}

	private void AnimateIn() {
		DialogStretcher.sizeDelta = new Vector2(Mathf.Clamp(StretcherBaseWidth + StretcherWidthPerCharacter * curStep.Line.TextString.Length, 600f, 1700f), DialogStretcher.sizeDelta.y);
		curState = State.FadingIn;
		Animator.Play("Hidden to Idle");
	}
	
	// Called by animator
	public void FinishAnimateIn() {
		curState = State.Hidden;
	}

	private void AnimateOut() {
		curState = State.FadingOut;
		curStep = null;
		Animator.Play("Idle to Hidden");
		MusicManager.Instance.RemoveFactor(MusicDuck);
		ducked = false;
	}
	
	// Called by animator
	public void FinishAnimateOut() {
		curState = State.Hidden;
		StepForward();
	}

	private void Update() {
		timer += Time.deltaTime;

		if (curStep != null) {
			
			// Animate in text
			Text.text = curStep.Line.TextString.Substring(0, (int)(curStep.Line.TextString.Length * Mathf.Clamp01(CurStepProgress * textSpeedMultiplier * curStep.Line.textSpeedMultiplier)));
			
			// Check for deletion
			if (CurStepProgress >= 1f) {
				AnimateOut();
			}
			
		}
	}
	
}

public class DialogStep {

	public DialogLine Line;
	public float Duration;

	public DialogStep(DialogLine line, float duration) { 
		Line = line;
		Duration = duration;
	}

}
