﻿using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;

public class RumbleManager : MonoBehaviour {

    private Rewired.Player player;
    
    public bool Test;
    public float TestTrauma;
    public float DecayRate = 0.5f;
	
    public static RumbleManager Instance;

    private float traumaR = 0f;
    private float traumaL = 0f;

    private void Awake() {
        Instance = this;
        player = ReInput.players.GetPlayer(0); 
    }

    private void Update() {

        if (Test) {
            Test = false;
            AddTrauma(TestTrauma);
        }
		
        player.SetVibration(0, traumaL);
        player.SetVibration(1, traumaR);

        traumaL = MathUtilities.Decay(traumaR, Time.deltaTime, DecayRate);
        traumaR = MathUtilities.Decay(traumaR, Time.deltaTime, DecayRate);
        
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="trauma">strength of rumble</param>
    /// <param name="pan">-1 = left only; 1 = right only; 0 is split evenly between left and right.</param>
    public void AddTrauma(float trauma, float pan = 0f) {
        trauma = Mathf.Clamp01(trauma);
        traumaR += Mathf.Clamp01(trauma * 0.5f + pan * 0.5f);
        traumaL += Mathf.Clamp01(-(trauma * -0.5f + pan * 0.5f));
    }
	
}

