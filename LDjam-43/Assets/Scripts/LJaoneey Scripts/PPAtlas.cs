﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PPAtlas : MonoBehaviour {

	public static PPAtlas Ins;

	private void Awake() { Ins = this; }

	public PostProcessVolume Pause;
	public PostProcessVolume Revival;
	public PostProcessVolume BloodDeath;
	public PostProcessVolume FireDeath;

}
