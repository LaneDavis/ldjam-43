﻿using System.Collections;
using System.Collections.Generic;
using System.Timers;
using Rewired;
using TMPro;
using UnityEngine;
using UnityEngine.Rendering.PostProcessing;

public class PauseManager : MonoBehaviour {

	private Player player;

	public AnimationCurve VolumeCurve;
	private float pauseTimer;
	
	private void Start() { player = Rewired.ReInput.players.GetPlayer(0); }

	private bool isPaused = false;

	public TextMeshProUGUI PauseText;
	public AnimationCurve TextFadeInCurve;
	public AnimationCurve TextSineCurve;
	public CanvasGroup CanvasGroup;

	private void Update() {

		if (isPaused) {
			pauseTimer += Time.unscaledDeltaTime;
			
			if (ReInput.players.GetPlayer(0).GetButtonDown("ResetLevel") && !TransitionController.Instance.IsDoingTransition) {
				TransitionController.Instance.ResetLevel();
				Unpause();
			}
		}

		PPAtlas.Ins.Pause.weight = VolumeCurve.Evaluate(pauseTimer);
		CanvasGroup.alpha = TextFadeInCurve.Evaluate(pauseTimer);
		PauseText.color = new Color(1f, 1f, 1f, TextSineCurve.Evaluate(pauseTimer));
		
		if (player.GetButtonDown("Pause")) {
			if (!isPaused) {
				Pause();
			} else if (isPaused) {
				Unpause();
			}
		}
		
	}

	private void Pause() {
		isPaused = true;
		Time.timeScale = 0;
		pauseTimer = 0f;
		MusicManager.Instance.SetLowPass(true);
	}

	private void Unpause() {
		isPaused = false;
		Time.timeScale = 1;
		pauseTimer = 0f;
		MusicManager.Instance.SetLowPass(false);
	}

}
