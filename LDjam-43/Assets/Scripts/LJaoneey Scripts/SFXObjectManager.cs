﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class SFXObjectManager : MonoBehaviour {

	public static SFXObjectManager Instance;
	
	public List<GameObject> SFXPrefabs = new List<GameObject>();

	private void Awake() { Instance = this; }	
	
	public void MakeSFXAtPoint(string SFXName, Vector2 point) {
		Instantiate(SFXPrefabs.First(e => e.name == SFXName), point, Quaternion.identity, null);
	}

}
	
