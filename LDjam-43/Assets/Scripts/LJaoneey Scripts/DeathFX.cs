﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathFX : SFXObject {

	public enum PPTarget {
		BloodDeath,
		FireDeath,
	}

	public PPTarget TargetPP;
	public SoundCall DelaySound;
	public float DelayTime = 0.6f;
	
	public AnimationCurve PPCurve;

	public override void StartFX() {
		base.StartFX();
		
		RumbleManager.Instance.AddTrauma(2f, 0f);
		Camshake.Instance.AddTrauma(1.5f);
		SoundManager.thisSoundManager.PlaySoundLater(DelaySound, DelayTime);
	}

	public override void Update() {
		base.Update();
		if (isOn) {
			if (TargetPP == PPTarget.BloodDeath) {
				PPAtlas.Ins.BloodDeath.weight = PPCurve.Evaluate(Progress);
			}
			
			else if (TargetPP == PPTarget.FireDeath) {
				PPAtlas.Ins.FireDeath.weight = PPCurve.Evaluate(Progress);
			}
		}
	}
	
}
