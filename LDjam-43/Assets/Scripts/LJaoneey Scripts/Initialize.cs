﻿using System.Collections;
using System.Collections.Generic;
using Rewired;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Initialize : MonoBehaviour {

	public static Initialize Instance;
	public List<GameObject> ObjectsToDestroy;

	[HideInInspector] public bool HasVisualIntroToConsume = false;
	[HideInInspector] public string InitializedOnLevel = "";

	private const string PersistentSceneName = "PersistentScene";

	private void Awake() {

		foreach (GameObject g in ObjectsToDestroy) {
			Destroy(g);
		}
		
		if (Instance != null) {
			Destroy(gameObject);
			return;
		} else {
			Instance = this;
		}
		
		bool PersistentSceneLoaded = TransitionController.Instance != null;
		for (int i = 0; i < SceneManager.sceneCount; i++) {
			if (SceneManager.GetSceneAt(i).name == PersistentSceneName) {
				PersistentSceneLoaded = true;
			}
		}

		if (!PersistentSceneLoaded) {
			LoadPersistentScene();
			HasVisualIntroToConsume = true;
			InitializedOnLevel = SceneManager.GetActiveScene().name;
		}

	}

	private void LoadPersistentScene() { SceneManager.LoadSceneAsync(PersistentSceneName, LoadSceneMode.Additive); }

}
