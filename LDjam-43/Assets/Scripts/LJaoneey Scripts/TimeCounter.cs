﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TimeCounter : MonoBehaviour {

	public static TimeCounter Instance;
	private TextMeshProUGUI Text;
	public float TimeSeconds;

	private void Awake() {
		Text = GetComponent<TextMeshProUGUI>();
		Instance = this;
	}

	private void Update() {
		
		TimeSeconds = Time.time;
		Text.text = TimeString();

	}

	public string TimeString() {

		int hours = (int) TimeSeconds / 3600;
		if (hours > 0) TimeSeconds -= hours * 3600;
		int minutes = (int) TimeSeconds / 60;
		TimeSeconds -= minutes * 60;
		float overflow = TimeSeconds % 1f;
		int intSeconds = (int) TimeSeconds;

		if (hours > 0) {
			return hours.ToString("D2") + ":" + minutes.ToString("D2") + ":" + intSeconds.ToString("D2") + overflow.ToString("F2").Substring(1, 3);
		} else {
			return minutes.ToString("D2") + ":" + intSeconds.ToString("D2") + overflow.ToString("F2").Substring(1, 3);
		}
		
	}
	
}
