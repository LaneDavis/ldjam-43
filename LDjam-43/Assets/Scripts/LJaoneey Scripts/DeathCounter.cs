﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class DeathCounter : MonoBehaviour {

	public static DeathCounter Instance;

	public TextMeshProUGUI Text;
	public List<FxPackageController> IncrementFX;
	public int DeathCount;

	public bool Test;

	private void Awake() { Instance = this; }

	public void AddDeath() {
		DeathCount++;
		Text.text = "x" + DeathCount;
		foreach (FxPackageController fx in IncrementFX) {
			fx.TriggerAll();
		}
	}

	private void Update() {
		if (Test) {
			Test = false;
			AddDeath();
		}
	}
	
}
