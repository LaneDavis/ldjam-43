﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(CatRecorder))]
public class EnvironmentInteraction : MonoBehaviour
{
    private Vector3 _startPos;
    public GameObject GhostPrefab;
    public GameObject DeadPrefab;
    private GameObject _ghostContainer;
    private bool _isCurrentlyRespawning;

    public float InvincibleDuration = .25f;
    public float RespawnDuration = 1.5f;
    private float _endInvincibleTime;

    public float SquishThreshold = 100;

    private void Start()
    {
        _startPos = transform.position;
        _ghostContainer = new GameObject("Ghosts");
        SceneManager.MoveGameObjectToScene(_ghostContainer, gameObject.scene);
        _endInvincibleTime = Time.time + InvincibleDuration;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (_endInvincibleTime < Time.time && other.CompareTag("Hazard"))
        {
            Respawn();
        }
    }

    private void OnCollisionStay2D(Collision2D coll)
    {
        if (coll.GetContact(0).normalImpulse >= SquishThreshold)
        {
            Respawn();
        }

        if (coll.gameObject.CompareTag("Dead"))
        {
            Vector2 dist = coll.transform.position - transform.position;
            if (dist.sqrMagnitude < .25f)
                Respawn();
        }
    }

    private void Respawn()
    {
        if (_isCurrentlyRespawning)
            return;
        
        _isCurrentlyRespawning = true;
        if (SFXObjectManager.Instance != null) {
            SFXObjectManager.Instance.MakeSFXAtPoint("Death Blood SFX", transform.position);
        }

        DeathCounter.Instance.AddDeath();
        DialogManager.Instance.QueueNextDeathDialog();
        
        StartCoroutine("DoRespawn");
    }

    private IEnumerator DoRespawn() {
        
        GameObject renderObj = GetComponentInChildren(typeof(MeshRenderer)).gameObject;
        Vector3 deathPosition = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        deathPosition.x -= .5f;
        deathPosition.y -= .5f;
        deathPosition.x = Mathf.Round(deathPosition.x);
        deathPosition.y = Mathf.Round(deathPosition.y);
        deathPosition.x += .5f;
        deathPosition.y += .5f;
        
        renderObj.SetActive(false);
        transform.position = new Vector3(1000, 1000, 1000);    // This is a perfect line of code
        var deadCat = Instantiate(DeadPrefab, deathPosition, Quaternion.identity, _ghostContainer.transform);
        PlayerManager.Instance.IsAlive = false;
        
        var recorder = GetComponent<CatRecorder>();
        recorder.MarkDeath();
        
        yield return new WaitForSeconds(RespawnDuration);
        
        Destroy(deadCat);
        transform.position = _startPos;
        PlayerManager.Instance.IsAlive = true;
        renderObj.SetActive(true);
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        var ghost = Instantiate(GhostPrefab, transform.position, Quaternion.identity, _ghostContainer.transform);
        ghost.name = "Ghost @" + Time.time;
        ghost.GetComponent<GhostCat>().Playback(recorder);
        _ghostContainer.BroadcastMessage("ResetLevel");
        transform.parent.parent.BroadcastMessage("ResetLevel", SendMessageOptions.DontRequireReceiver);
        _endInvincibleTime = Time.time + InvincibleDuration;
        
        if (SFXObjectManager.Instance != null) {
            SFXObjectManager.Instance.MakeSFXAtPoint("Revival SFX", transform.position);
        }
        
        _isCurrentlyRespawning = false;
    }
}
