using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Tilemaps;

[CreateAssetMenu]
public class GunTile : GOCustomizableTile<GunInfo>
{
    protected override void EnsureExistsAt(Vector3 position)
    {
        GunInfo.GetGunInfoAt(position, true);
    }

    protected override void DestroyAt(Vector3 position)
    {
        GunInfo.DestroyGunInfoAt(position);
    }

    protected override GunInfo GetAt(Vector3 position)
    {
        return GunInfo.GetGunInfoAt(position, false);
    }

    protected override void ApplyCustomization(GunInfo gunInfo, ref TileData tileData)
    {
        tileData.color = gunInfo.StartOff ? Color.white : Color.red;
        tileData.transform = gunInfo.FacingLeft ? Matrix4x4.Scale(new Vector3(-1, 1, 1)) : Matrix4x4.identity;
        tileData.flags = TileFlags.LockAll;
        tileData.colliderType = Tile.ColliderType.None;
    }

    protected override void ClearCustomization(ref TileData tileData)
    {
        tileData.color = Color.white;
        tileData.transform = Matrix4x4.identity;
        tileData.flags = TileFlags.LockAll;
        tileData.colliderType = Tile.ColliderType.None;
    }
}