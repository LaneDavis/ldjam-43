﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelEndDetector : MonoBehaviour {

	public SoundCall DoorOpen;
	public float Radius;
	private bool StartedTransition;

	private void Update() {
		if (PlayerManager.Instance.playerObject != null &&
		    Vector2.Distance(PlayerManager.Instance.playerObject.transform.position, transform.position) < Radius &&
		    !StartedTransition) {
			
			GetComponentInParent<Animator>().SetTrigger("open");
			
			// Only do this once
			StartedTransition = true;
			
			// Play sounds
			SoundManager.instance.PlaySound(DoorOpen, gameObject);
		
			// Submit score
			float time = TimeCounter.Instance.TimeSeconds;
			int deaths = DeathCounter.Instance.DeathCount;
			LeaderboardSubmission submission = new LeaderboardSubmission(null, 1, time, deaths);
			LeaderboardManager.Instance.SubmitScore(submission);
			
			// Go to next level
			TransitionController.Instance.StartLevelOutro(transform.position);
		}
	}
}
