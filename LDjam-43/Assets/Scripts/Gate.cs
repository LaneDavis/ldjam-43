﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour {

    public SoundCall GateOpen;
    public SoundCall GateClosed;

    private bool isOn = false;
    
    public void TurnOn()
    {
        if (!isOn) {
            GetComponentInChildren<Animator>().SetBool("GateOpen", true);
            if (SoundManager.instance != null) {
                SoundManager.instance.PlaySound(GateOpen, gameObject);
            }

            isOn = true;
        }
    }

    public void TurnOff()
    {
        if (isOn) {
            GetComponentInChildren<Animator>().SetBool("GateOpen", false);
            if (SoundManager.instance != null) {
                SoundManager.instance.PlaySound(GateClosed, gameObject);
            }

            isOn = false;
        }
    }
    
    
}
