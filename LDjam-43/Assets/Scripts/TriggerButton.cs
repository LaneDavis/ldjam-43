﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class TriggerButton : MonoBehaviour, IInteractionSetup
{
	public List<GameObject> Targets;
	public LayerMask ValidSources;
	public SoundCall ButtonPress;
	public SoundCall ButtonRelease;

	public List<Collider2D> CurrentlyTouching = new List<Collider2D>();
	private bool wasOnLastFrame = false;
	private bool isOn = false;

	private void OnTriggerEnter2D(Collider2D other)
	{
		if ((ValidSources.value & (1 << other.gameObject.layer)) != 0) {
			CurrentlyTouching.Add(other);
			GetComponentInChildren<Animator>().SetBool("Pressed", true);
			
			// Play sound
			if (SoundManager.instance != null)
				SoundManager.instance.PlaySound(ButtonPress, gameObject);
		}
	}

	private ContactPoint2D[] _contactPointBuffer = new ContactPoint2D[1];
	private void OnTriggerExit2D(Collider2D other)
	{
		if (GetComponent<Collider2D>().GetContacts(_contactPointBuffer) == 0) {
			if (CurrentlyTouching.Contains(other)) {
				CurrentlyTouching.Remove(other);
			}

			if (CurrentlyTouching.Count <= 0 && isOn) {
				GetComponentInChildren<Animator>().SetBool("Pressed", false);
			}
			
			// Play sound
			if (SoundManager.instance != null)
				SoundManager.instance.PlaySound(ButtonRelease, gameObject);
		}
	}

	private void Update() {
		isOn = CurrentlyTouching.Count > 0;

		if (isOn && !wasOnLastFrame) {
			foreach (var target in Targets)
			{
				if (gameObject.transform.IsChildOf(target.transform))
					continue;
				target.SendMessage("TurnOn", SendMessageOptions.RequireReceiver);
			}
		} else if (!isOn && wasOnLastFrame) {
			foreach (var target in Targets)
			{
				if (gameObject.transform.IsChildOf(target.transform))
					continue;
				target.SendMessage("TurnOff", SendMessageOptions.RequireReceiver);
			}
		}

		wasOnLastFrame = isOn;
	}

	//Called by animation
	public void On()
	{
//		foreach (var target in Targets)
//		{
//			if (gameObject.transform.IsChildOf(target.transform))
//				continue;
//			target.SendMessage("TurnOn", SendMessageOptions.RequireReceiver);
//		}
	}
	
	//Called by animation
	public void Off()
	{
//		foreach (var target in Targets)
//		{
//			if (gameObject.transform.IsChildOf(target.transform))
//				continue;
//			target.SendMessage("TurnOff", SendMessageOptions.RequireReceiver);
//		}
	}

	public void SetupInteraction(List<GameObject> targets)
	{
		Targets = targets;
	}
}
