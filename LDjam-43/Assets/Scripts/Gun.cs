﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class Gun : MonoBehaviour
{
    public Transform MainBody;
    public GameObject BulletPrototype;
    public Transform Muzzle;
    
    private bool _on;
    private GunInfo _info;
    
    public SoundCall CannonFire;

    private void Start()
    {
        _info = GunInfo.GetGunInfoAt(transform.position, true);
        MainBody.rotation = Quaternion.Euler(0, _info.FacingLeft ? 180 : 0, 0);
        _on = !_info.StartOff;
    }

    private float _shootTime;
    private void Update()
    {
        #if UNITY_EDITOR
        if (!Application.isPlaying)
            return;
        #endif
        if (!_on)
            return;
        if (Time.time >= _shootTime)
        {
            _shootTime = Time.time + _info.Delay;
            var bullet = Instantiate(BulletPrototype, Muzzle.transform.position, Muzzle.transform.rotation, null);
            bullet.GetComponent<Bullet>().Speed = _info.Speed;
            bullet.SetActive(true);
            
            // Play sound
            if (SoundManager.instance != null)
                SoundManager.instance.PlaySound(CannonFire, gameObject);
        }
    }

    private void ResetLevel()
    {
        _shootTime = 0;
    }
    
    public void TurnOn()
    {
        _on = _info.StartOff;
    }

    public void TurnOff()
    {
        _on = !_info.StartOff;
    }
}
