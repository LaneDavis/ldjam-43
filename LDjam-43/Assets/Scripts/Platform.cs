﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

public class Platform : MonoBehaviour, IInteractionSetup
{
    public float Speed;

    private Vector3 _trueFirstPosition;
    private PlatformWaypoint _nextTarget;
    private PlatformWaypoint _firstTarget;
    private Rigidbody2D _body;

    public GameObject CannonLocation;
    public GameObject CannonLeftPrefab;
    public GameObject CannonRightPrefab;
    

    public bool On = true;
    private void Start()
    {
        _trueFirstPosition = transform.position;
        _firstTarget = _nextTarget = PlatformWaypoint.GetPathAt(transform.position);
        _body = GetComponent<Rigidbody2D>();
        ResetLevel();
    }

    private void FixedUpdate()
    {
        if (On) {
            if (_nextTarget == null)
                return;
            Vector2 distanceToTarget = _nextTarget.transform.position - transform.position;
            float delta = Speed * Time.deltaTime;
            if (distanceToTarget.sqrMagnitude <= delta * delta) {
                _nextTarget = _nextTarget.Next;
            } else {
                _body.velocity = distanceToTarget.normalized * Speed;
            }
        } else {
            _body.velocity = Vector3.zero;
        }
    }

    private void ResetLevel()
    {
        if (_firstTarget == null)
            return;
        _nextTarget = _firstTarget;
        transform.position = _trueFirstPosition;
        _body.velocity = Vector2.zero;
    }
    
    public void TurnOn()
    {
        On = true;
    }

    public void TurnOff()
    {
        On = false;
    }

    public void MakeCannonLeft() {
        if (CannonLocation != null && CannonLeftPrefab != null) {
            GameObject newCannon = Instantiate(CannonLeftPrefab);
            newCannon.transform.position = CannonLocation.transform.position;
            newCannon.transform.SetParent(transform);
        }
    }

    public void MakeCannonRight() {
        if (CannonLocation != null && CannonRightPrefab != null) {
            GameObject newCannon = Instantiate(CannonRightPrefab);
            newCannon.transform.position = CannonLocation.transform.position;
            newCannon.transform.SetParent(transform);
        }
    }

    public void SetupInteraction(List<GameObject> targets)
    {
        On = false;
    }
}

#if UNITY_EDITOR
[CustomEditor(typeof(Platform))]
public class PlatformEditor : Editor
{		
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        
        var platform = (Platform)target;
        if(GUILayout.Button("Make Cannon Left")) {
            platform.MakeCannonLeft();
        }

        if (GUILayout.Button("Make Cannon Right")) {
            platform.MakeCannonRight();
        }
    }
}
#endif
