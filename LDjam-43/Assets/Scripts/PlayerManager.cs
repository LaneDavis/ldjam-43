﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour {

	public static PlayerManager Instance;
	[HideInInspector] public bool IsAlive = true; 

	private void Awake() { Instance = this; }

	[HideInInspector] public GameObject playerObject;

	public GameObject PlayerObject {
		get {
			if (playerObject != null) return playerObject;
			CatRecorder cr = FindObjectOfType<CatRecorder>();
			if (cr != null) {
				playerObject = cr.gameObject;
				return playerObject;
			}

			return null;
		}
	}

	private void Update() {
		if (playerObject == null) {
			CatRecorder cr = FindObjectOfType<CatRecorder>();
			if (cr != null) {
				playerObject = cr.gameObject;
			}
		}
	}
	
}
