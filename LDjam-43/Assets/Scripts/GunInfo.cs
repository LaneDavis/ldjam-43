using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

[ExecuteInEditMode]
public class GunInfo : MonoBehaviour
{
    public bool FacingLeft;
    public float Delay = .5f;
    public float Speed = 5;
    public bool StartOff;
    
    private static HashSet<GunInfo> _infos = new HashSet<GunInfo>();

    public static GunInfo GetGunInfoAt(Vector2 pos, bool create)
    {
        Vector2Int intPos = Vector2Int.RoundToInt(pos);
        var ret = _infos.FirstOrDefault(e => e.IntPos == intPos);
        if (ret == null && create)
        {
            GameObject go = new GameObject("Gun Info @" + pos);
            go.transform.position = pos;
            ret = go.AddComponent<GunInfo>();
        }
        return ret;
    }

    public static void DestroyGunInfoAt(Vector2 pos)
    {
        var info = _infos.FirstOrDefault(e => e.IntPos == pos);
        if (info != null)
            DestroyImmediate(info.gameObject);
    }

    private void OnEnable()
    {
        _infos.Add(this);
        if (transform.parent == null)
        {
            GameObject parent = GameObject.Find("Guns");
            if (parent == null)
                parent = new GameObject("Guns");
            transform.parent = parent.transform;
        }
    }

#if UNITY_EDITOR
    private void OnValidate()
    {
        foreach (var tilemap in FindObjectsOfType<Tilemap>())
        {
            if (tilemap.GetTile(IntPos3) is GunTile) {
                tilemap.RefreshTile(IntPos3);
            }
        }
    }
    #endif

    private Vector2Int IntPos
    {
        get { return Vector2Int.RoundToInt(transform.position); }
    }
    
    private Vector3Int IntPos3
    {
        get { return Vector3Int.RoundToInt(transform.position); }
    }


    private void OnDestroy()
    {
        _infos.Remove(this);
    }
}