﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class CatRecorder : MonoBehaviour
{
	private readonly List<Vector2> _positions = new List<Vector2>();
	private readonly List<bool> _facingDirs = new List<bool>();
	private bool _isRecording = true;
	
	public void CopyAndResetPath(out Vector2[] positions, out bool[] facingDirs)
	{
		positions = _positions.ToArray();
		facingDirs = _facingDirs.ToArray();
		_positions.Clear();
		_facingDirs.Clear();
		_isRecording = true;
	}

	public void MarkDeath()
	{
		_isRecording = false;
	}

	private void FixedUpdate()
	{
		if (_isRecording && !TransitionController.Instance.IsDoingTransition)
		{
			_positions.Add(transform.position);
			_facingDirs.Add(transform.localScale.x > 0);
		}
	}
}
