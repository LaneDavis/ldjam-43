﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

public class LeaderboardManager : MonoBehaviour
{
	public List<LeaderboardSO> LeaderboardData;
	private string LeaderboardFilePath = "Assets/Data/";
	public float DeathScore = 10;
	public float TimeScorePerSecond = 1;
	private bool _loaded;

	public static LeaderboardManager Instance;

	private void Awake()
	{
		Instance = this;
	}

	// Submits a score to the leaderboard
	public void SubmitScore(LeaderboardSubmission submission)
	{
//		if (!_loaded) Load();
//		
//		// Calculate score based on deaths and time. Low scores are better
//		submission.Score = (long)(DeathScore * submission.NumDeaths + TimeScorePerSecond * submission.LevelTime);
//
//		LeaderboardData[submission.Level - 1].submissions.Add(submission);
//
//		Save();
	}

	public List<LeaderboardSubmission> GetScores(int level)
	{
		// Get data before getting level scores
		if (!_loaded) Load();
		
		List<LeaderboardSubmission> scores = LeaderboardData[level - 1].submissions;
		return scores.OrderByDescending(x => x.Score).ToList();
	}

	public void Save()
	{
		// Load old data before saving new data
		if (!_loaded) Load();

		// Each leaderboard is stored as JSON in its own file, suffixed by _lvlNumber
		for (int i = 0; i < LeaderboardData.Count; i++)
		{
			string asJson = JsonUtility.ToJson(LeaderboardData[i], true);
			File.WriteAllText(GetLeaderboardFilePath(i), asJson, Encoding.UTF8);	
		}
	}

	public void Load()
	{
		_loaded = true;
				
		// Each leaderboard is stored as JSON in its own file, suffixed by _lvlNumber
		for (int i = 0; i < LeaderboardData.Count; i++)
		{
			try
			{
				string json = File.ReadAllText(GetLeaderboardFilePath(i), Encoding.UTF8);
				List<LeaderboardSubmission> leaderboardSubmissions =
					JsonUtility.FromJson<List<LeaderboardSubmission>>(json);
				if (leaderboardSubmissions == null) return;

				LeaderboardData[i].submissions = leaderboardSubmissions;
			}
			catch (FileNotFoundException ex)
			{
				// File doesn't exist, so create it
				File.WriteAllText(GetLeaderboardFilePath(i), "");
			}
		}
	}

	public void ResetScores()
	{
		// Reset submissions for all leaderboards
		for (int i = 0; i < LeaderboardData.Count; i++)
		{
			LeaderboardData[i].submissions = new List<LeaderboardSubmission>();
		}
		
		// Save changes
		Save();
	}

	private string GetLeaderboardFilePath(int level)
	{
		return LeaderboardFilePath + "leaderboard_" + level + ".json";
	}
}


#if UNITY_EDITOR
[CustomEditor(typeof(LeaderboardManager))]
public class LeaderboardEditor : Editor
{		
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
        
		var leaderboardManager = (LeaderboardManager)target;
		if(GUILayout.Button("Reset Leaderboards"))
		{
			leaderboardManager.ResetScores();
		}
	}
}
#endif
