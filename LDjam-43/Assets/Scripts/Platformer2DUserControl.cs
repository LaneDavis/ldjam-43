using System;
using Rewired;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

namespace UnityStandardAssets._2D
{
    [RequireComponent(typeof (PlatformerCharacter2D))]
    public class Platformer2DUserControl : MonoBehaviour
    {
        private PlatformerCharacter2D m_Character;
        private float m_maxJumpStartTime;

        private void Awake()
        {
            m_Character = GetComponent<PlatformerCharacter2D>();
        }

        private void Update()
        {
            if (ReInput.players.GetPlayer(0).GetButtonDown("Jump"))
            {
                m_maxJumpStartTime = Time.time + .1f;
            }
        }

        private void FixedUpdate()
        {
            // Read the inputs.
            bool crouch = Input.GetKey(KeyCode.LeftControl);
            bool jump = ReInput.players.GetPlayer(0).GetButton("Jump");
            float h = CrossPlatformInputManager.GetAxis("Horizontal");
            bool startJump = m_maxJumpStartTime > Time.time;
            // Pass all parameters to the character control script.
            m_Character.Move(h, crouch, startJump, jump);
            if (!jump)
                m_maxJumpStartTime = 0;
        }
    }
}
