﻿using UnityEngine;

public class DoorEntrance : MonoBehaviour
{
	public SoundCall DoorOpen;
	private bool _entered;

	private void Start()
	{
		SoundManager.instance.PlaySound(DoorOpen, gameObject);
		GetComponent<Animator>().SetTrigger("open");
	}

	public void AlertCat()
	{
		FindObjectOfType<CatDoorInteraction>().AnimateThroughDoor(transform, true);
	}
}
