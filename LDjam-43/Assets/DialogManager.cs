﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogManager : MonoBehaviour {

	public static DialogManager Instance;
	
	public DialogData Data;

	private void Awake() {
		Instance = this;
		TimeOfNextIdleDialog = Time.time + TimeToIdleDialog;
		foreach (DialogLine line in Data.LevelLines) {
			line.PlayerHasHeardLine = false;
		}
	}

	// Level Start Dialog Delay
	private const float LevelStartDialogDelay = 2f;
	private List<DialogLine> queuedLevelStartLines = new List<DialogLine>();
	private float levelStartDialogPlayTime = 0f;
	private bool stillNeedsToTryLevelStartForThisLevel = false;

	// Idle Dialog
	private const float TimeToIdleDialog = 60f;
	private float TimeOfNextIdleDialog;
	
	private int CurLevel { get { return TransitionController.lastLevel; } }
	
	public void QueueNextLevelStartDialog() {

		levelStartDialogPlayTime = Time.time + LevelStartDialogDelay;
		stillNeedsToTryLevelStartForThisLevel = true;
		int safetyForTesting = 0;
		while (safetyForTesting < 5) {
			DialogLine nextLine = NextUnheardDialog(DialogLine.DialogTrigger.LevelStart);
			if (nextLine == null || queuedLevelStartLines.Contains(nextLine)) {
				break;
			} else {
				queuedLevelStartLines.Add(nextLine);
			}
			safetyForTesting++;
		}
		
	}
	
	public void QueueNextDeathDialog() {

		DialogLine line = NextUnheardDialog(DialogLine.DialogTrigger.Death);
		if (line != null) {
			DialogBoxController.Instance.QueueDialog(line);
			TimeOfNextIdleDialog = Time.time + TimeToIdleDialog;
		}

	}
	
	public void QueueNextRestartDialog() {

		DialogLine line = NextUnheardDialog(DialogLine.DialogTrigger.Restart);
		if (line != null) {
			stillNeedsToTryLevelStartForThisLevel = true;
			levelStartDialogPlayTime = Time.time + LevelStartDialogDelay * 0.75f;
			queuedLevelStartLines.Add(line);
		}

	}
	
	private DialogLine NextUnheardDialog(DialogLine.DialogTrigger triggerType) {
		int level = CurLevel;
		foreach (DialogLine line in Data.LevelLines) {
			if ((line.LevelNum == level || line.LevelNum == -1) && !line.PlayerHasHeardLine && line.Trigger == triggerType) {
				line.PlayerHasHeardLine = true;
				return line;
			}
		}
		return null;
	}

	private void Update() {
		if (Time.time > levelStartDialogPlayTime && stillNeedsToTryLevelStartForThisLevel) {
			stillNeedsToTryLevelStartForThisLevel = false;
			foreach (DialogLine line in queuedLevelStartLines) {
				DialogBoxController.Instance.QueueDialog(line);
				TimeOfNextIdleDialog = Time.time + TimeToIdleDialog;
			}
			queuedLevelStartLines.Clear();
		}

		if (Time.time > TimeOfNextIdleDialog) {
			DialogLine line = NextUnheardDialog(DialogLine.DialogTrigger.Idle);
			if (line != null) {
				DialogBoxController.Instance.QueueDialog(line);
				TimeOfNextIdleDialog = Time.time + TimeToIdleDialog;
			}
		}
		
	}

	private void OnDestroy() {
		foreach (DialogLine line in Data.LevelLines) {
			line.PlayerHasHeardLine = false;
		}
	}
	
}
