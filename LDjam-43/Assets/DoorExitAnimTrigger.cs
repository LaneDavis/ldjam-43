﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorExitAnimTrigger : MonoBehaviour {
	public void AlertCat()
	{
		FindObjectOfType<CatDoorInteraction>().AnimateThroughDoor(transform, false);
	}
}
